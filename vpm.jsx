import React from 'react';

import { useSelector } from 'react-redux';

function TurtleOSViewport() {
    const currentView = useSelector(state=>state.vpm);
    return (
        <div style={{
            background: '#fff',
            width: '100%',
            height: '100%',
            position: 'fixed',
            top:0,
            left:0,
        }}>
          {currentView}
        </div>
       );
}

class ViewIndexManager {
    constructor(index) {
        this.index=index;
    }
    set(index) {
        this.index=index;
    }
    get() {
        return this.index;
    }
}
export {
    TurtleOSViewport,
    ViewIndexManager
}
